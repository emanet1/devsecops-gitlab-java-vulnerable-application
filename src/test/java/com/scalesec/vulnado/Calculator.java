public class Calculator {
    public int addition(String input) {
        String[] numbers = input.split("\\+");
        int sum = 0;
        for (String number : numbers) {
            sum += Integer.parseInt(number);
        }
        return sum;
    }
}
